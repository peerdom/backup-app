
LN = ln -sfv

BORG_VERSION ?= 1.1.10
BORG_URL = https://github.com/borgbackup/borg/releases/download/$(BORG_VERSION)/borg-linux64
BORG = bin/borg

RCLONE_VERSION ?= 1.50.2
RCLONE_URL = https://downloads.rclone.org/v$(RCLONE_VERSION)/rclone-v$(RCLONE_VERSION)-linux-amd64.zip
RCLONE = bin/rclone

.PHONY: install
install: $(BORG) $(RCLONE)

$(BORG): $(BORG)-$(BORG_VERSION)
	$(LN) "$(shell pwd)/$<" $@

$(BORG)-$(BORG_VERSION):
	mkdir -p bin
	curl -s --output $@ -L $(BORG_URL)
	chmod +x $@
	$@ -V
	touch $@

$(RCLONE): $(RCLONE)-$(RCLONE_VERSION)
	$(LN) "$(shell pwd)/$<" $@

$(RCLONE)-$(RCLONE_VERSION):
	mkdir -p bin
	mkdir -p tmp-rclone
	curl -s --output tmp-rclone/rclone.zip -L $(RCLONE_URL)
	unzip tmp-rclone/rclone.zip -d tmp-rclone
	cp tmp-rclone/rclone-v$(RCLONE_VERSION)-linux-amd64/rclone $@
	chmod +x $@
	$@ version
	rm -rf tmp-rclone

.PHONY: docker-test
docker-test: ## Run local tests in docker
	mkdir -p to-backup
	echo "Backme up if you can" > to-backup/just-that-file
	docker run -v $(shell pwd):/app \
			       -e BORG_REPOSITORY_PATH=/app/backup/borg-repository \
			       -e PATH_TO_BACKUP=/app/to-backup \
						 -e	BORG_PASSPHRASE=testpassphrase \
						 registry.gitlab.com/peerdom/backup-app:test-image \
						 make backup

.PHONY: backup
backup:
	make postgres-backup
	make borg-backup

.PHONY: borg-backup
borg-backup: $(BORG_REPOSITORY_PATH) $(BORG)
	$(BORG) create --compression zlib \
			--list \
			--stats \
			$<::"$(shell date -Isecond)" \
			$(PATH_TO_BACKUP)
	rm -rf $(PATH_TO_BACKUP)/postgres-db.psql

$(BORG_REPOSITORY_PATH): $(BORG)
	mkdir -p $@
	$(BORG) init --encryption repokey-blake2 $@

postgres-backup:
	pg_dump --compress=0 --format=c $(POSTGRESQL_ADDON_URI) -f $(PATH_TO_BACKUP)/postgres-db.psql

clean:
	rm -rf bin
	rm -rf tmp
	rm -rf tmp-rclone
